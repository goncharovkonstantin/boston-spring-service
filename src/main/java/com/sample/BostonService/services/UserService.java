package com.sample.BostonService.services;

import com.sample.BostonService.domain.User;

public interface UserService {
    User createUser(User user);

    void delete(String email);

    User findUserByEmail(String email);
}
