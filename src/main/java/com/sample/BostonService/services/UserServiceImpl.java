package com.sample.BostonService.services;

import com.sample.BostonService.domain.User;
import com.sample.BostonService.exceptions.InvalidEmailException;
import com.sample.BostonService.exceptions.UserExistsException;
import com.sample.BostonService.exceptions.UserNotFoundException;
import com.sample.BostonService.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void delete(String email) {
        if (findUserByEmail(email) != null) {
            userRepository.deleteByEmail(email);
        }
    }

    @Override
    public User findUserByEmail(String email) {
        return userRepository.findUserByEmail(email).orElseThrow(() -> new UserNotFoundException());
    }

    public User createUser(User user) {
        if (user.getEmail() == null)
            throw new InvalidEmailException();

        boolean exists = userRepository.findUserByEmail(user.getEmail()).isPresent();
        if (exists)
            throw new UserExistsException();

        user.setPassword(user.getPassword());
        return userRepository.save(user);
    }
}
