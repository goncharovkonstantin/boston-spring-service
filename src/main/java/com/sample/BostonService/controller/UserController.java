package com.sample.BostonService.controller;

import com.sample.BostonService.domain.User;
import com.sample.BostonService.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("/users")
@Validated
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/add")
    public User addUser(@RequestBody @Valid User user)
    {
        User res = userService.createUser(user);
        res.setPassword(null);
        return res;
    }

    @GetMapping("/{email}")
    public User getUser(@PathVariable String email)
    {
        User res = userService.findUserByEmail(email);
        res.setPassword(null);
        return res;
    }

    @DeleteMapping("/{email}")
    public ResponseEntity deleteUser(@PathVariable String email)
    {
        userService.delete(email);
        return ResponseEntity.ok().build();
    }
}
